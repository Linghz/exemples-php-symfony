<?php

namespace AG\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response ;
use AG\FrontBundle\Entity\Route;
use AG\FrontBundle\Form\RouteType;
use Symfony\Component\Filesystem\Filesystem;

class ConfigController extends Controller
{
    public function indexAction(Request $request)
    {
        //var_dump($request);
        $em = $this->getDoctrine()->getManager('navigation');
        $routes = $em->getRepository('AGFrontBundle:Route')->findAll();
        return $this->render('AGFrontBundle:Config:config-routes.html.twig', array('routes' => $routes));

    }

    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager('navigation');
        $route = new Route();
        $form = $this->createForm('AG\FrontBundle\Form\RouteType', $route);

        if($request->isMethod('POST') && $form->handleRequest($request)->isValid() )
        {
//            $em = $this->getDoctrine()->getManager('navigation');
            //test création de fichier
            $datas = $request->request->all();
            foreach ($datas as $elem1 )
            {
                foreach ($elem1 as $key => $value)
                {
                    if($key =='url')
                    {
                        $titrepage = $value;
                    }
                }
            }

            $fs = new Filesystem();
            $file = $titrepage ;
//            $uploaddir = $route->getPagesDir();
//            $file = $uploaddir.$titrepage ;
            $myfile = fopen("pages/".$file, "w");

            //ecritures des include mais prevoir systeme de templating
            $txt = "{% extends \"@webpages/layout/layout-body.html.twig\" %}\n";
//            $txt = "{% extends \"AGFrontBundle::layout.html.twig\" %}\n";
            fwrite($myfile, $txt);
            $txt = "{% block front_view %}\n";
            fwrite($myfile, $txt);
            $txt = "\tContenu de la page ".$titrepage."\n";
            fwrite($myfile, $txt);
            $txt = "{% endblock %}\n";
            fwrite($myfile, $txt);
            fclose($myfile);

            if ($fs->exists("pages/".$file)) // EN COURS D'ECRITURE
            {
                $newfile = "pages/sauvegardes/".md5(uniqid()).$titrepage;
                $fs->copy("pages/".$file,$newfile, true);
            }
            //return new response();
            $em->persist($route);
            $em->flush();
            $request->getSession()->getFlashBag()->add('config-route', 'Route bien créé.');
            return $this->redirectToRoute('ag_config_routes');
        }

        return $this->render('AGFrontBundle:Config:add-routes.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager('navigation');
        $route = $em->getRepository('AGFrontBundle:Route')->find($id);
        $form = $this->createForm('AG\FrontBundle\Form\RouteType', $route);
        $oldfile = $route->getUrl();

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid())
        {

            $em->persist($route);
            $em->flush();
            $newNamefile =  $route->getUrl();

            $fs = new Filesystem();
            if ($fs->exists("pages/".$oldfile))
            {
                $fs->rename("pages/".$oldfile, "pages/".$newNamefile);
            }

            $request->getSession()->getFlashBag()->add('config-route', 'Route bien modifié.');
            return $this->redirectToRoute('ag_config_routes');
        }
        return $this->render('AGFrontBundle:Config:edit-routes.html.twig', array('route' => $route, 'form' => $form->createView()));
    }



    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager('navigation');
        $routeDelete = $em->getRepository('AGFrontBundle:Route')->find($id);
        if(null === $routeDelete)
        {
            throw new NotFoundHttpException("Cette route n'existe pas");
        }
        else
        {
            $em->remove($routeDelete);
            $em->flush();
            $request->getSession()->getFlashBag()->add('config-route', "La route a été supprimée");
            return $this->redirectToRoute('ag_config_routes');
        }
    }

}
