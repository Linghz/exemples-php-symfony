<?php

namespace AG\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response ;
use AG\ModulesBundle\Entity\Champs;
use AG\ModulesBundle\Form\ChampsType;
use AG\AdminBundle\Entity\Products;
use AG\AdminBundle\Entity\Image;
use AG\AdminBundle\Entity\Datatitre;
use AG\AdminBundle\Entity\Datatva;
use AG\AdminBundle\Entity\Datatexte;
use AG\AdminBundle\Entity\Databool;
use AG\AdminBundle\Entity\Datanombre;
use AG\AdminBundle\Entity\Datadate;
use AG\AdminBundle\Entity\Categorie;
use AG\AdminBundle\Form\ProductsType;
use AG\AdminBundle\Form\DatatitreType;
use AG\AdminBundle\Form\DatadateType;
use AG\AdminBundle\Form\ImageType;

class ModuleFrontController extends Controller
{

    public function popupAction($page, $moduleid, Request $request)
    {
        $popupservice = $this->container->get('ag_front.datarecords');
        $popups = $popupservice->rsByModule($moduleid);

        return $this->render('@webpages/modules/popup/module-inc.html.twig', array(
            'popups' => $popups
        ));
    }

    public function livredorAction($page, $moduleid, $request)
    {

        // CONFIGURATION FORMULAIRE D'ENVOI
        $em = $this->getDoctrine()->getManager();
        $champsLivredor = $em->getRepository('AGModulesBundle:Champs')->getByModuleActif($moduleid); // a changer l'id si supression du module
        $product = new Products();
        $formLivredor = $this->createForm('AG\AdminBundle\Form\ProductsType', $product);

        //TRAITEMENT FORMULAIRE
        if ($request->isMethod('POST') && $formLivredor->handleRequest($request)->isValid())
        {
            $user = null;
            if ($this->getUser() != null)
            {
                $user = $this->getUser()->getId();
            }

            //récupération des parametres
            $datas = $request->request->all();
            $moduleid = $request->request->get('module');

            //appel et utilisation du service d'enregistrement des données
            $parentid = null;
            $dataRecords = $this->container->get('ag_front.datarecords');
            $dataRecords->enregistrement($moduleid,$product,$datas,$user, $parentid);

            //retour vers la page livre d'or
            $request->getSession()->getFlashBag()->add('livredor', 'Message bien envoyé, il sera validé par un administrateur avant publication.');
            $this->get('router')->generate('ag_front_pages', array(
                'page' => $page,
                'moduleid' => $moduleid,
                'champsLivredor' => $champsLivredor
            ));
        }

        $livredorservice = $this->container->get('ag_front.datarecords');
        $messages = $livredorservice->rsByModule($moduleid);
        return $this->render('@webpages/modules/livredor/module-inc.html.twig', array(
            'messages' => $messages,
            'formLivredor' => $formLivredor->createView(),
            'champsLivredor' => $champsLivredor,
            'moduleid' => $moduleid,
            'page' => $page
        ));
    }


    public function contactAction($page, $moduleid, $request )
    {
        // CONFIGURATION FORMULAIRE D'ENVOI
        $em = $this->getDoctrine()->getManager();
        $allChampFormContact = $em->getRepository('AGModulesBundle:Champs')->getByModuleActif($moduleid); // a changer l'id si supression du module
        $product = new Products();
        $formContact = $this->createForm('AG\AdminBundle\Form\ProductsType', $product);

        //TRAITEMENT FORMULAIRE DE CONTACT
        if ($request->isMethod('POST') && $formContact->handleRequest($request)->isValid())
        {
            //récupération des parametres
            $datas = $request->request->all();
            $moduleid = $request->request->get('module');

            //appel et utilisation du service d'enregistrement des données
            $dataRecords = $this->container->get('ag_front.datarecords');
            $parentid = null;
            $user = null;
            $dataRecords->enregistrement($moduleid,$product,$datas, $user, $parentid);

            // RESTE A FAIRE L'ENVOI DE MAIL
            $name = "Greg";
            $message = \Swift_Message::newInstance()
                ->setSubject('Formulaire de contact')
                ->setFrom('gregory@web-portail.com')
                ->setTo('gregory@arpaline.net')
                ->setBody(
                    $this->renderView(
                    // app/Resources/views/Emails/registration.html.twig
                        'Emails/testmail.html.twig',
                        array('name' => $name)
                    ),
                    'text/html'
                )
                /*
                 * If you also want to include a plaintext version of the message
                ->addPart(
                    $this->renderView(
                        'Emails/registration.txt.twig',
                        array('name' => $name)
                    ),
                    'text/plain'
                )
                */
            ;
            $this->get('mailer')->send($message);
            //retour vers la page contact
            $request->getSession()->getFlashBag()->add('contact', 'Message bien envoyée.');
            $this->get('router')->generate('ag_front_pages', array(
                'page' => $page,
                'moduleid' => $moduleid,
                'champsLivredor' => $allChampFormContact
            ));

        }
        //FIN TRAITEMENT FORMULAIRE DE CONTACT

        return $this->render('@webpages/modules/contact/module-inc.html.twig', array(
            'formContact' => $formContact->createView(),
            'allChampFormContact' => $allChampFormContact,
            'moduleid' => $moduleid,
            'page' => $page
        ));
    }

    public function ecommerceAction($page, $moduleid, Request $request, $categorie, $scat1, $scat2, $scat3, $scat4, $id)
    {
        //recup variable session panier // voir si on peut le mettre ailleurs de facon a ce que cela soit global
        $session = $request->getSession();
        if($session->has('panier'))
        {
            $panier = $session->get('panier');
        }
        else
        {
            $panier = false;
        }


        if($id != NULL) // fiche détail
        {

            $produitservices = $this->container->get('ag_front.datarecords');
            $produits = $produitservices->rsByProduit($id);

            //récupération des infos de la fiche produit
            $em = $this->getDoctrine()->getManager();
            //recup infos id déclinaison

            $ficheProduit = $em->getRepository('AGAdminBundle:Products')->find($id);


            //récupération des catégories
            $categories = $em->getRepository('AGAdminBundle:Categorie')->findBy(array(
                'moduleid' => $moduleid,
                'niveau' => 'categorie',
                'actif' => true
            ));

            return $this->render('@webpages/modules/ecommerce/module-inc.html.twig', array(
                'produits' => $produits,
                'moduleid' => $moduleid,
                'page' => $page,
                'categories' => $categories,
                'fichedetail' => '1',
                'panier' => $panier,
                'ficheProduit' => $ficheProduit,

            ));
        }
        else // fiches résumés
        {
            $produitservices = $this->container->get('ag_front.datarecords');
            $produits = $produitservices->rsByCat($moduleid, $categorie, $scat1, $scat2, $scat3, $scat4);

            $em = $this->getDoctrine()->getManager();
            $categories = $em->getRepository('AGAdminBundle:Categorie')->findBy(array(
                'moduleid' => $moduleid,
                'niveau' => 'categorie',
                'actif' => true
            ));

            return $this->render('@webpages/modules/ecommerce/module-inc.html.twig', array(
                'produits' => $produits,
                'moduleid' => $moduleid,
                'page' => $page,
                'categories' => $categories,
                'panier' => $panier
            ));
        }
    }
    public function optionSelectAction($idfichemere)
    {
        $em = $this->getDoctrine()->getManager();
        $listeficheOptions =  $em->getRepository('AGAdminBundle:Products')->findBy(array('idnum' => $idfichemere));
        $fiche = $em->getRepository('AGAdminBundle:Products')->find($idfichemere);
        $configOption = "";
        $configOption = $fiche->getDecli();

        $produitservices = $this->container->get('ag_front.datarecords');

        // liste des options
        $tabOpt =[];

        foreach ($listeficheOptions as $opt)
        {
            $tabOpt[$opt->getId()] = $opt->getId();
            $moduleid = $opt->getModuleId();
        }
        $options = $produitservices->rsByArray($tabOpt);

        $splitDecli = explode(">>",$configOption );
        $cpt = 0;
        foreach($splitDecli as $elem )
        {
            //on enleve les espaces de début et de fin
            $splitDecli[$cpt] = trim($elem) ;
            $cpt++;
        }

        //traitement pour 1 seule déclinaison
        $nbopt = count($listeficheOptions);
        $iduniquedecli =$listeficheOptions[0]->getId();
        $tabElemUnique = [];
        $stockPositif = false;
        $prix1 = 0;
        $prix2 = 0;
        if ($nbopt == 1)
        {
            foreach($splitDecli as $elem )
            {
                //requete pour récupérer prix et stock
                $prix1 = $em->getRepository('AGAdminBundle:Datanombre')->findBy(array(
                    'products' => $iduniquedecli,
                    'champConfig' => 'prix1'
                ));
                $prix2 = $em->getRepository('AGAdminBundle:Datanombre')->findBy(array(
                    'products' => $iduniquedecli,
                    'champConfig' => 'prix2'
                ));
                // on détermine si le stock est supérieur a 0
                $fichestock1 = $em->getRepository('AGAdminBundle:Datanombre')->findBy(array(
                    'products' => $iduniquedecli,
                    'champConfig' => 'stock1'
                ));
            }
            if ($fichestock1[0]->getData()>0)
            {
                $stockPositif = true;
            }
        }



        //problématique : récupérer la liste des titres et autres champs pour le premier select
        $distinctChamps = $em->getRepository('AGAdminBundle:datatitre')->distinctTitre($tabOpt);

        //configuration des options
        $configChamps = $em->getRepository('AGModulesBundle:Champs')->getByModule($moduleid);
        return $this->render('@webpages/modules/ecommerce/options-inc.html.twig', array(
            'idfichemere' => $idfichemere,
            'options' => $options,
            'splitDecli' => $splitDecli,
            'configChamps' => $configChamps,
            'distinctChamps' => $distinctChamps,
            'listeficheOptions' => $listeficheOptions,
            'nbopt' => $nbopt,
            'stockPositif' => $stockPositif,
            'prix1' => $prix1,
            'prix2' => $prix2,


        ));
    }

    public function ajaxOptionAction(Request $request)
    {
        //conditionnelle temporaire pour les TEST . a commenter une fois les tests en ligne
        if($request->isMethod('POST'))
        {
            $dataChamp = $request->request->get('dataChamp');
            $champConfig = $request->request->get('champConfig');
            $idfichemere = $request->request->get('idfichemere');
            $cptSelect = $request->request->get('cptSelect');
        }
        else
        {
            $dataChamp = $request->query->get('dataChamp');
            $champConfig = $request->query->get('champConfig');
            $idfichemere = $request->query->get('idfichemere');
            $cptSelect = $request->query->get('cptSelect');

        }

        $em = $this->getDoctrine()->getManager();

        //On doit récupérer la liste des options  du second select
        $listeficheOptions =  $em->getRepository('AGAdminBundle:Products')->findBy(array('idnum' => $idfichemere));
        $fiche = $em->getRepository('AGAdminBundle:Products')->find($idfichemere);
        $produitservices = $this->container->get('ag_front.datarecords');

        $tabOpt =[];
        foreach ($listeficheOptions as $opt)
        {
            $tabOpt[$opt->getId()] = $opt->getId();
        }

        $options = $produitservices->rsByArray($tabOpt);
        $splitDecli = explode(">>",$fiche->getDecli() );

        $cpt = 0;
        $tabConfigOpt = [];
        $tabSplit = [];
        foreach($splitDecli as $elem )
        {
            //on enleve les espaces de début et de fin
            $tabSplit[$cpt] = trim($elem) ;
            $cpt++;
        }

        //Génération du xml de retour
        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml.='<declinaison>';

        //on doit déterminer si il  y a un autre select a afficher en fonction de la config
        if ($cptSelect+1 < count($tabSplit) )
        {
            $elemNext = $tabSplit[$cptSelect+1]; // élément dapres dans la config

            // on doit récuperer le moduleid // A FAIRE
            $moduleid = 13;

            // A VERIFIER mais récupération de l'ordre des champs titre // voir a peut etre faire la requete manuellement
            // en spécifiant l'ordre
            $configmodule = $em->getRepository('AGModulesBundle:Champs')->findBy(array('module' =>$moduleid ));
            $cptChamp = 0 ;
            foreach($configmodule as $cft )
            {
                if($cft->getType() == "titre")
                {
                    $tabConfigOpt[$cft->getNom()] = $cptChamp;
                    $cptChamp++;
                }
            }

            //compteur option suivante dans la configuration des déclinaison
            $cptElemNext = $tabConfigOpt[$elemNext];
            $tabValue = [];


            foreach($options as $key => $ficheSousJacente) // boucle sur les résultats
            {

                if($ficheSousJacente['titre'][$tabConfigOpt[$champConfig]]->getChampConfig() == $champConfig && $ficheSousJacente['titre'][$tabConfigOpt[$champConfig]]->getData() == $dataChamp) // doit déterminer le bon indice en fonction de la config
                {
                    if (!array_key_exists($ficheSousJacente['titre'][$cptElemNext]->getData(), $tabValue)) {
                        $tabValue[$ficheSousJacente['titre'][$cptElemNext]->getData()] = $ficheSousJacente['titre'][$cptElemNext]->getData();

                        //on détermine les infos de prix
                        //on a l'id de la fiche produits  ?
                        $ficheprix1 = $em->getRepository('AGAdminBundle:Datanombre')->findBy(array(
                            'products' => $key,
                            'champConfig' => 'prix1'
                        ));
                        // on détermine si le stock est supérieur a 0
                        $fichestock1 = $em->getRepository('AGAdminBundle:Datanombre')->findBy(array(
                            'products' => $key,
                            'champConfig' => 'stock1'
                        ));
                        $xml.='<champs>';
                        $xml.='<libelle>'.$tabValue[$ficheSousJacente['titre'][$cptElemNext]->getData()].'</libelle>';
                        $xml.='<id>'.$ficheSousJacente['titre'][$cptElemNext]->getId().'</id>';
//                        if($fichestock1[0]->getData() > 0)
//                        {
//                            $xml.='<epuise>non</epuise>';
//                        }
//                        else
//                        {
//                            $xml.='<epuise>oui</epuise>';
//                        }
                        $xml.='</champs>';
                    }
                }
            }
            $xml.='<fin>non</fin>';
        }
        else
        {

            $xml.='<champs>';
            $xml.='<libelle></libelle>';
            $xml.='<prix></prix>';
            $xml.='<id></id>';
            $xml.='</champs>';
            $xml.='<fin>oui</fin>';
        }

        $xml.='</declinaison>';
        $response = new Response($xml);
        $response->headers->set('Content-Type', 'application/xml; charset=utf-8');
        return $response;

        return $this->render('@webpages/modules/ecommerce/ajax.html.twig', array(
            'options' => $options,
            'tabValue' => $tabValue,
            'tabConfigOpt' => $tabConfigOpt,

        ));
    }

    public function ajaxResultAction(Request $request)
    {
        //conditionnelle temporaire pour les TEST . a commenter une fois les tests en ligne
        if($request->isMethod('POST'))
        {
            $idoption = $request->request->get('idoption');
        }
        else
        {
            $idoption = $request->query->get('idoption');
        }
        $em = $this->getDoctrine()->getManager();
        $champTitre = $em->getRepository('AGAdminBundle:Datatitre')->find($idoption);
        $idProduit = $champTitre->getProducts()->getId();
//        var_dump($champTitre->getProducts()->getId());
        $infosprix = $em->getRepository('AGAdminBundle:Datanombre')->findBy(array('products' => $idProduit));
        $temporary = $em->getRepository('AGAdminBundle:Products')->findOneBy(array('id' => $idProduit));
        $prod = $em->getRepository('AGAdminBundle:Products')->findOneBy(array('id' => $temporary->getIdnum()));



        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml.='<infosprix>';

        //traitement pour savoir quel prix prendre
        $prixAutiliser = 0;
        $idprixAutiliser ='';
        if (array_key_exists('1', $infosprix) && array_key_exists('2', $infosprix))
        {
            if ($infosprix[1]->getData() > $infosprix[2]->getData() )
            {
                $prixAutiliser = $infosprix[2]->getData();
                $idprixAutiliser = $infosprix[2]->getId();
            }
            elseif ($infosprix[1]->getData() > $infosprix[2]->getData() )
            {
                $prixAutiliser = $infosprix[1]->getData();
                $idprixAutiliser = $infosprix[1]->getId();
            }
            elseif($infosprix[1]->getData() == $infosprix[2]->getData())
            {
                if($infosprix[2]->getData() > 0)
                {
                    $prixAutiliser = $infosprix[2]->getData();
                    $idprixAutiliser = $infosprix[2]->getId();
                }
                else
                {
                    $prixAutiliser = $infosprix[1]->getData();
                    $idprixAutiliser = $infosprix[1]->getId();
                }

            }
            else
            {
                $prixAutiliser = $infosprix[1]->getData();
                $idprixAutiliser = $infosprix[1]->getId();
            }
        }
        ////////////////////////////////////////////////////////

        if (array_key_exists('1', $infosprix))
        {
            $xml.='<prix1>'.$infosprix[1]->getData().'</prix1>'; // $infosprix[1] = prix1
        }

        if (array_key_exists('2', $infosprix))
        {
            $xml.='<prix2>'.$infosprix[2]->getData().'</prix2>'; // $infosprix[2] = prix2
        }

        //on vérifie si il y a des stock
        if ($infosprix[0]->getData()<= 0) // $infosprix[0] = stock
        {
            $xml.='<epuise>oui</epuise>';
        }
        else
        {
            $xml.='<epuise>non</epuise>';
        }

        $xml.='<prixoptions>'.$prod->getInfoprix().'</prixoptions>';
        $xml.='<idprix>'.$idprixAutiliser.'</idprix>';
        $xml.='</infosprix>';
        $response = new Response($xml);
        $response->headers->set('Content-Type', 'application/xml; charset=utf-8');
        return $response;

        // pour débugage
//        return $this->render('@webpages/modules/ecommerce/ajax2.html.twig', array(
//            'prod' => $prod,
//        ));
    }

}
